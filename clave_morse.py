import time
import unicodedata
import winsound
# Codigo de traducción de letras a morse
codigo = {
    'A': '.-',     'B': '-...',    'C': '-.-.',
    'D': '-..',    'E': '.',       'F': '..-.',
    'G': '--.',    'H': '....',    'I': '..',
    'J': '.---',   'K': '-.-',     'L': '.-..',
    'M': '--',     'N': '-.',      'O': '---',
    'P': '.--.',   'Q': '--.-',    'R': '.-.',
    'S': '...',    'T': '-',       'U': '..-',
    'V': '...-',   'W': '.--',     'X': '-..-',
    'Y': '-.--',   'Z': '--..',    '1': '.----',
    '2': '..---',  '3': '...--',   '4': '....-',
    '5': '.....',  '6': '-....',   '7': '--...',
    '8': '---..',  '9': '----.',   '0': '-----',
    '.': '.-.-.-', ',': '--..--',  ':': '---...',
    ';': '-.-.-.', '?': '..--..',  '!': '-.-.--',
    '"': '.-..-.', "'": '.----.',  '+': '.-.-.',
    '-': '-....-', '/': '-..-.',   '=': '-...-',
    '_': '..--.-', '$': '...-..-', '@': '.--.-.',
    '&': '.-...',  '(': '-.--.',   ')': '-.--.-'
}
#------------------------------------------------------------------------------------------
'''def convierte_ascii(c):
    """Convierte c a una cadena que únicamente contiene
    caracteres ASCII.
    
    Convierte caracteres con acento, diéresis o tilde al
    carácter simple correspondiente. Elimina cualquier otro
    carácter que no sea ASCII.
    """
    return (unicodedata.normalize('NFD', c.decode('utf8'))
            .encode('ascii', 'ignore'))
'''
#------------------------------------------------------------------------------------------
#Entrada: mensaje escrito por el usuario
#Que hace: Convierte mensaje a una cadena de símbolos morse.
#Salida: Lista de caracteres
def convierte_morse(mensaje):    
    resultado = []
    for c in mensaje.upper():
        if c in codigo:
            resultado.append(codigo[c])
        elif c == ' ':
            resultado.append('  ')
    return resultado
    #return ' '.join(resultado)
#------------------------------------------------------------------------------------------
#Entrada: ninguna
#Que hace: Se produce un audio que le corresponde a cada caracter
#Salida: ninguna
def sonido():
    oracion= str(input("Escriba lo que desea traducir a clave morse: "))
    lista= convierte_morse(oracion)
    print(convierte_morse(oracion))
    for letra in lista:
        for signo in letra:
            if signo== ' ':
                time.sleep(2.1456)
            elif signo=='-':
                winsound.PlaySound("morse2.wav", winsound.SND_FILENAME)
                time.sleep(0.2384)
                #if signo+1!= '  ':
                    #time.sleep(0.2384)
            if signo=='.':
                winsound.PlaySound("morse1.wav", winsound.SND_FILENAME)
                time.sleep(0.2384)
                #if signo+1 != '  ':
                    #time.sleep(0.2384)
            


sonido()
